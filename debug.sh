#!/bin/bash
set -eu
lines="$(awk '/int main/{ print NR; exit }' 42.cc)"
head "-$((lines - 1))" < 42.cc > hdr.h
g++ -g -Wall -Wextra -std=c++17 tests.cc -o tests && echo "compiled" || (echo "err"; exit 1)
gdb ./tests