#include "hdr.h"
#include <iostream>
#include <climits>
using namespace std;

void test_hash() {
   LazyCalculator calc;
   for (int i = CHAR_MIN; i <= CHAR_MAX; ++i) {
      char c = static_cast<char>(i);
      if (string("*+/-024").find(c) != string::npos) continue;
      //cout << c << " <- " << i << endl;
      calc.define(c, [i](Lazy a, Lazy b) { a(); b(); return i; });
      string s = "00";
      s += c;
      assert(calc.calculate(s) == i);
   }
   cout << "hashing OK" << endl;
}

void test_lazy() {
   LazyCalculator calc;
   string state;
   calc.define('!', [&state](Lazy a, Lazy b) { 
      state += to_string(a());
      state += to_string(b());
      return 0;
   });
   Lazy l = calc.parse("42!");
   assert(state.empty());
   assert(l() == 0);
   assert(state == "42");
   l();
   assert(state == "4242");
   state = "";
   assert(calc.calculate("42!2!") == 0);
   assert(state == "4202");
   cout << "laziness OK" << endl;
}

void test_not_too_lazy() {
   LazyCalculator calculator;
   for (auto bad: {"", "42", "4+", "424+"}) {
        try {
            calculator.parse(bad);
            assert(false);
        }
        catch (SyntaxError) {
        }
    }

    calculator.define('!', [](Lazy a, Lazy b) { return a()*10 + b(); });
    try {
        calculator.define('!', [](Lazy a, Lazy b) { return a()*10 + b(); });
        assert(false);
    }
    catch (OperatorAlreadyDefined) {
    }

    try {
        calculator.define('0', [](Lazy, Lazy) { return 0; });
        assert(false);
    }
    catch (OperatorAlreadyDefined) {
    }

    try {
        calculator.parse("02&");
        assert(false);
    }
    catch (UnknownOperator) {
    }

    cout << "not too laziness OK" << endl;
}

int main() {
   test_hash();
   test_lazy();
   test_not_too_lazy();
   cout << "OK" << endl;
   return 0;
}