#include <stack>
#include <string>
#include <functional>
#include <exception>
#include <cassert>

class SyntaxError : public std::exception {
    const char* what() const noexcept override {
        return "Syntax Error";
    }
};

class OperatorAlreadyDefined : public std::exception {
    const char* what() const noexcept override {
        return "Operator Already Defined";
    }
};

class UnknownOperator : public std::exception {
    const char* what() const noexcept override {
        return "Unknown Operator";
    }
};

using Lazy = std::function<int(void)>;

class LazyCalculator {
    private:
        static constexpr size_t char_range = 1 << (8 * sizeof(char));

        /*
        Expression is a function that handles parsing a literal or an operator.
        It gets a reference to the stack, 
        pops its arguments from there (if any)
        and pushes its (lazy) result to the stack.
        */
        using Expression = std::function<void(std::stack<Lazy>&)>;
        Expression expressions[char_range];

        inline unsigned int hash(char c) const {
            return static_cast<unsigned char>(c);
        }

        inline Expression get_expression(char c) const {
            return expressions[hash(c)];
        }

        bool is_defined(char c) const {
            return get_expression(c) != nullptr;
        }

        /*
        Literal is an Expression that just pushes a lazy value to the stack.
        */
        Expression make_literal(int x) const {
            return [x](std::stack<Lazy>& stack) {
                stack.push([x]() { return x; });
            };
        }

        /*
        Operator is an Expression that pops its lazy arguments from the stack
        and pushes a lazily computed result of applying its operator 
        to the arguments.
        */
        Expression make_operator(std::function<int(Lazy, Lazy)> op) const {
            return [op](std::stack<Lazy>& stack) {
                if (stack.size() < 2) {
                    throw SyntaxError{};
                }

                Lazy b = stack.top();
                stack.pop();
                Lazy a = stack.top();
                stack.pop();

                stack.push(bind(op, a, b));
            };
        }

        void define_expression(char c, Expression expr) {
            if (is_defined(c)) {
                throw OperatorAlreadyDefined{};
            }

            expressions[hash(c)] = expr;
        }

    public:
        LazyCalculator() {
            define_expression('0', make_literal(0));
            define_expression('2', make_literal(2));
            define_expression('4', make_literal(4));

            define('+', [](Lazy a, Lazy b) { return a() + b(); });
            define('-', [](Lazy a, Lazy b) { return a() - b(); });
            define('*', [](Lazy a, Lazy b) { return a() * b(); });
            define('/', [](Lazy a, Lazy b) { return a() / b(); });
        }

        Lazy parse(const std::string& s) const {
            std::stack<Lazy> stack;
            for (size_t i = 0; i < s.length(); ++i) {
                char c = s[i];
                if (is_defined(c)) {
                    get_expression(c)(stack);
                } else {
                    throw UnknownOperator{};
                }
            }

            if (stack.size() != 1) {
                throw SyntaxError{};
            }
            return stack.top();
        }

        int calculate(const std::string& s) const {
            return parse(s)();
        }

        void define(char c, std::function<int(Lazy, Lazy)> fn) {
            define_expression(c, make_operator(fn));
        }
};

std::function<void(void)> operator*(int n, std::function<void(void)> fn) {
    return [=]() {
        for (int i = 0; i < n; i++)
            fn();
    };
}

int manytimes(Lazy n, Lazy fn) {
    (n() * fn)();  // Did you notice the type cast?
    return 0;
}

int main() {
    LazyCalculator calculator;

    // The only literals...
    assert(calculator.calculate("0") == 0);
    assert(calculator.calculate("2") == 2);
    assert(calculator.calculate("4") == 4);

    // Built-in operators.
    assert(calculator.calculate("42+") == 6);
    assert(calculator.calculate("24-") == -2);
    assert(calculator.calculate("42*") == 8);
    assert(calculator.calculate("42/") == 2);

    assert(calculator.calculate("42-2-") == 0);
    assert(calculator.calculate("242--") == 0);
    assert(calculator.calculate("22+2-2*2/0-") == 2);

    // The fun.
    calculator.define('!', [](Lazy a, Lazy b) { return a()*10 + b(); });
    assert(calculator.calculate("42!") == 42);

    std::string buffer;
    calculator.define(',', [](Lazy a, Lazy b) { a(); return b(); });
    calculator.define('P', [&buffer](Lazy, Lazy) { buffer += "pomidor"; return 0; });
    assert(calculator.calculate("42P42P42P42P42P42P42P42P42P42P42P42P42P42P42P4"
                                "2P,,,,42P42P42P42P42P,,,42P,42P,42P42P,,,,42P,"
                                ",,42P,42P,42P,,42P,,,42P,42P42P42P42P42P42P42P"
                                "42P,,,42P,42P,42P,,,,,,,,,,,,") == 0);
    assert(buffer.length() == 42 * std::string("pomidor").length());

    std::string buffer2 = std::move(buffer);
    buffer.clear();
    calculator.define('$', manytimes);
    assert(calculator.calculate("42!42P$") == 0);
    // Notice, how std::move worked.
    assert(buffer.length() == 42 * std::string("pomidor").length());

    calculator.define('?', [](Lazy a, Lazy b) { return a() ? b() : 0; });
    assert(calculator.calculate("042P?") == 0);
    assert(buffer == buffer2);

    assert(calculator.calculate("042!42P$?") == 0);
    assert(buffer == buffer2);

    calculator.define('1', [](Lazy, Lazy) { return 1; });
    assert(calculator.calculate("021") == 1);

    for (auto bad: {"", "42", "4+", "424+"}) {
        try {
            calculator.calculate(bad);
            assert(false);
        }
        catch (SyntaxError) {
        }
    }

    try {
        calculator.define('!', [](Lazy a, Lazy b) { return a()*10 + b(); });
        assert(false);
    }
    catch (OperatorAlreadyDefined) {
    }

    try {
        calculator.define('0', [](Lazy, Lazy) { return 0; });
        assert(false);
    }
    catch (OperatorAlreadyDefined) {
    }

    try {
        calculator.calculate("02&");
        assert(false);
    }
    catch (UnknownOperator) {
    }

    return 0;
}
