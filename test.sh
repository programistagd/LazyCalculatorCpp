#!/bin/bash
set -eu
lines="$(awk '/int main/{ print NR; exit }' 42.cc)"
head "-$((lines - 1))" < 42.cc > hdr.h
g++ -Wall -Wextra -O2 -std=c++17 tests.cc -o tests && echo "compiled" || (echo "err"; exit 1)
./tests